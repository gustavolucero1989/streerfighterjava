package stage;

import fighter.CommonFighter;

public class Map {
	private CommonFighter fighterRight;
	private CommonFighter fighterLeft;
	private int roundTime;
	
	public Map ( CommonFighter fighterRight, CommonFighter fighterLeft, int roundTime) {
		this.fighterRight= fighterRight;
		this.fighterLeft = fighterLeft;
		this.roundTime= roundTime;
		
	}
	public void checkStatus () {
		if (this.fighterRight.getLife()>0 && this.fighterLeft.getLife()> 0  )
		{
			this.phaseAttack();
			this.sideEffects();
			this.checkStatus();
			
		}else if (this.fighterLeft.getLife()>0){
			this.fighterRight.sayState();
			this.fighterLeft.sayState();
			System.out.println("\nWINNER IS "+this.fighterLeft.getName());
		}else {
			this.fighterRight.sayState();
			this.fighterLeft.sayState();
			System.out.println("\nWINNER IS "+this.fighterRight.getName());
		}
	}
	
	public void phaseAttack() {
			if (this.fighterRight.getSpeedAttack()>this.fighterLeft.getSpeedAttack())
			{
				this.fighterRight.sayState();
				this.fighterLeft.sayState();
				
				for(int i=0;i< (roundTime/fighterRight.getSpeedAttack());i++ ) {
				this.fighterRight.attackFighter(fighterLeft,playerDistance());
				}
				
				this.fighterRight.sayState();
				this.fighterLeft.sayState();
				
				for(int i=0;i< (roundTime/fighterLeft.getSpeedAttack());i++ ) {
				this.fighterLeft.attackFighter(fighterRight,playerDistance());
				}
			}else {
				this.fighterRight.sayState();
				this.fighterLeft.sayState();
				
				for(int i=0;i< (roundTime/fighterLeft.getSpeedAttack());i++ ) {
				this.fighterLeft.attackFighter(fighterRight,playerDistance());
				}
				
				this.fighterRight.sayState();
				this.fighterLeft.sayState();
				
				for(int i=0;i< (roundTime/fighterRight.getSpeedAttack());i++ ) {
				this.fighterRight.attackFighter(fighterLeft,playerDistance());
				}
			}
			}
	
	public void sideEffects () {
			this.fighterRight.loadStaminaInAttack();
			this.fighterRight.loadStaminaInDefense();
			this.fighterLeft.loadStaminaInAttack();
			this.fighterLeft.loadStaminaInDefense();
			}
	
	private float playerDistance () {
		float max = Math.max(fighterRight.getPosition(), fighterLeft.getPosition());
		float min = Math.min(fighterRight.getPosition(), fighterLeft.getPosition());
		return (max-min);
	}
	
	public void iniciarCombate() {
		this.checkStatus();
		
	}

}
