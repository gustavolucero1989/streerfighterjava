package fighter;

public class RangedFighter extends CommonFighter {
	public RangedFighter (float life,float stamina,float attack, float mistycAttack, int defense, int speedAttack, float position, String name) {
		this.life = life;
		this.stamina = stamina;
		this.attack= attack;
		this.mistycAttack=mistycAttack;
		this.defense = defense;
		this.name = name;
		this.speedAttack= speedAttack;
		this.position=position;
		this.combatStyle = 2;
	}

	@Override
	public void loadStaminaInAttack() {
		if (this.stamina<100) {
		this.stamina += 20;
		}
		if (this.stamina>100) {
			this.stamina=100;
		}
	}
	@Override
	public void loadStaminaInDefense() {
		if (this.stamina<100) {
		this.stamina += 40;
	}
		if (this.stamina>100) {
			this.stamina=100;
		}
	}


	@Override
	
	public float bonusAttack(float playerDistance) {
		float bonus;
		bonus = (playerDistance/20)*10;
		bonus=this.attack + bonus;
		return bonus;
	}
	
}
