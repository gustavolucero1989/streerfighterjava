package fighter;

public class WizardFighter extends CommonFighter {
	public WizardFighter (float life, float stamina,float attack, float mistycAttack, int defense, int speedAttack, float position, String name) {
		this.life = life;
		this.stamina = stamina;
		this.attack=attack;
		this.mistycAttack= mistycAttack;
		this.defense = defense;
		this.speedAttack = speedAttack;
		this.position=position;
		this.name = name;
		this.combatStyle = 3;
	}

	@Override
	public void loadStaminaInAttack() {
	if (this.stamina<100) {
		this.stamina += 20;
	}
	if (this.stamina>100) {
		this.stamina=100;
	}
	}
	@Override
	public void loadStaminaInDefense() {
		if (this.stamina<100) {
		this.stamina += 30;
	}
		if (this.stamina>100) {
			this.stamina=100;
		}
	}


	@Override
	public float bonusAttack(float playerDistance) {
		float bonus;
		bonus = (playerDistance/20)*15;
		bonus=this.mistycAttack + bonus;
		return bonus;
	}
	
}