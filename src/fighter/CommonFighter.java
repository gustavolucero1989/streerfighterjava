package fighter;

public abstract class CommonFighter {
	protected float life;
	protected float stamina;
	protected float attack;
	protected float mistycAttack;
	protected float defense;
	protected int speedAttack;
	protected String name;
	protected float position;
	protected int combatStyle;
	

	
	public int getSpeedAttack() {
		return speedAttack;
	}

	public void setSpeedAttack(int speedAttack) {
		this.speedAttack = speedAttack;
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getStamina() {
		return stamina;
	}

	public void setStamina(float stamina) {
		this.stamina = stamina;
	}

	public float getMistycAttack() {
		return mistycAttack;
	}

	public void setMistycAttack(float mistycAttack) {
		this.mistycAttack = mistycAttack;
	}

	public float getDefense() {
		return defense;
	}

	public void setDefense(float defense) {
		this.defense = defense;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCombatStyle() {
		return combatStyle;
	}

	public void setCombatStyle(int combatStyle) {
		this.combatStyle = combatStyle;
	}

	public float getAttack() {
		return attack;
	}

	public void setAttack(float attack) {
		this.attack = attack;
	}
	

	public float getPosition() {
		return position;
	}

	public void setPosition(float position) {
		this.position = position;
	}
	
	
	//Crear Metodo


	public void sayName() {System.out.println("My name is: " + this.name);}
	public void sayStatus() {System.out.println("My life is: " + this.life);}
	public void sayStamina() {System.out.println("My stamina is: " +this.stamina);}
	public void sayAttack() {System.out.println("My attack is: " +this.attack);}
	public void sayCombatStyle() {System.out.println("My style is: " + this.combatStyle);}
	
	public void sayState () { 
		sayName();
		sayStatus();
		//sayAttack();
		sayStamina();
	}
	
	public void attackFighter(CommonFighter target, float playerDistance) {
		System.out.println("\n");
		if (this.life>0) {//Para que no ataque de vuelta despues de muerto 
			System.out.println(this.name+": Is My turn of attack!");
			System.out.println("My attack+bonus is: "+this.bonusAttack(playerDistance)+"\n");
			target.getDamage(this.bonusAttack(playerDistance));
		}
	}

	public void getDamage(float enemyAttack) {
			if (enemyAttack > this.defense) {
			this.life = this.life - (enemyAttack - this.defense);
		}
	}
	
	//Metodo abstracto no lleva llaver se implementa
	public abstract float bonusAttack(float playerDistance);
	public abstract void loadStaminaInAttack();
	public abstract void loadStaminaInDefense();
	
}