package app;

import fighter.PhysicalFighter;
import fighter.RangedFighter;
//import fighter.WizardFighter;
import stage.Map;

public class App {

	public static void main(String[] args) {
		//instanciar una variable de tipo clase
		PhysicalFighter pf = new PhysicalFighter(100, 0, 70, 0, 20, 20, 40 , "GUSTY");
		RangedFighter rf = new RangedFighter	(100, 0, 40, 0, 20, 10, 80 , "AGUS");
		//WizardFighter wf = new WizardFighter	(100, 0, 0, 50, 20, 40, 60 , "MARTIN");
		Map mp = new Map (pf,rf,30);		
		
		
		//usamos un metodo de la variable que instanciamos
		mp.iniciarCombate();
	}

}
